uvicorn app.main:app --reload

autopep8 --in-place --aggressive --aggressive *.py

docker build --no-cache -t administrator -f Dockerfile .

docker run -d --name administrator  --restart always -p 8001:8001/tcp  administrator:latest


redis://default:redispw@localhost:55000