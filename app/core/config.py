import os
from pathlib import Path
from dotenv import load_dotenv

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

class Settings:
    """It's a class that contains all the settings for the project"""
    PROJECT_NAME: str = "MEASUREMENTS"
    PROJECT_VERSION: str = "1.0"
    SQLALCHEMY_DATABASE_URL: str = os.getenv('SQLALCHEMY_DATABASE_URL')


settings = Settings()
