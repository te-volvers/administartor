from fastapi import APIRouter, status, Body, Depends, Path
from app.measurement.schema.measurement import Measurement, Analytics
from sqlalchemy.orm import Session
from app.db.database import get_db
from app.measurement.repository import measurement

from fastapi_pagination import Page, paginate

router = APIRouter(
    prefix="/measurement",
    tags=["Measurements"],
    responses={404: {"description": "Not found"}},
)


@router.post(
    '/add',
    status_code=status.HTTP_201_CREATED,
    response_model=Measurement,
    summary="Add Measurement",
)
async def add_measurement(
    measurement_body: Measurement = Body(...),
    db_session: Session = Depends(get_db),
):
    """
    Create Measurement

    This path operation Create Measurement in the app

    Parameters:
    Request body parameter
    - **measurement_body**: Measurement

    Returns a json with the basic Measurement information:
    - **measurement_body**: Measurement

    """
    return measurement.add_measurement(db_session, measurement_body)


@router.get(
    '/',
    response_model=Page[Measurement],
    status_code=status.HTTP_200_OK,
    summary='Get Measurement'
)
async def get_measurements(
    db_session: Session = Depends(get_db),
):
    """
    This path operation shows all Measurement in the app:

    Returns a json with the basic Measurement information:
    - **measurement**: [Measurement]
    """
    return paginate(measurement.get_measurements(db_session))


@router.get(
    '/{id_measurement}',
    response_model=Measurement,
    status_code=status.HTTP_200_OK,
    summary='Get Measurement by ID'
)
async def get_measurement(
    id_measurement: int = Path(..., example="1"),
    db_session: Session = Depends(get_db),

):
    """
    This path operation  get Measurement in the app:
    Parameters:
    - Request path parameter
    - **id_measurement**: str


    Returns a json with the basic Measurement information:
    - **measurement**: Measurement
    """
    return measurement.get_measurement(db_session, id_measurement)


@router.patch(
    '/{id_measurement}',
    status_code=status.HTTP_201_CREATED,
    summary="Update Measurement",
)
async def update_measurement(
    measurement_body: Measurement = Body(...),
    id_measurement: int = Path(..., example="1"),
    db_session: Session = Depends(get_db),
):
    """
    Update Measurement

    This path operation Update Measurement in the app

    Parameters:
    - Request path parameter
    - **id_measurement**: str
    - Request body parameter
    - **measurement_body**: Measurement

    Returns a json with the basic Measurement information:
    - **measurement_body**: Measurement

    """
    return measurement.update_measurement(db_session, measurement_body, id_measurement)


@router.delete(
    '/{id_measurement}',
    status_code=status.HTTP_201_CREATED,
    summary="Delete Measurement",
)
async def delete_measurement(
    id_measurement: int = Path(..., example="1"),
    db_session: Session = Depends(get_db),
):
    """
    Delete Measurement

    This path operation Delete Measurement in the app

    Parameters:
    - Request path parameter
    - **id_measurement**: str


    Returns a json with the basic Measurement information:
    - **measurement_body**: Measurement

    """
    return measurement.delete_measurement(db_session, id_measurement)


@router.post(
    '/csv',
    status_code=status.HTTP_200_OK,
    summary='Generate CSV Measurement'
)
async def generate_csv(
    days: int = Body(..., example=1),
    db_session: Session = Depends(get_db),
):
    """
    This path operation generated CSV Measurement in the app:

    Returns a json with the basic Measurement information:
    - String
    """
    return measurement.generate_csv(db_session, days)


@router.post(
    '/analytics',
    status_code=status.HTTP_200_OK,
    summary='Generate analysis Measurement'
)
async def data_analytics(
    body_analytics: Analytics = Body(...),

):
    """
    This path operation generated CSV Measurement in the app:

    Returns a json with the basic Measurement information:
    - String
    """
    return measurement.data_analytics(body_analytics)
