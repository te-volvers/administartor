from datetime import datetime, timedelta
from sqlalchemy.orm import Session
from fastapi import HTTPException, status
from app.measurement.schema.measurement import Measurement, Analytics
from app.measurement import models
import pandas as pd
import matplotlib.pyplot as plt


def add_measurement(db_session: Session, measurement: Measurement):
    """
    It takes a database session and a measurement object, converts the measurement object to a
    dictionary, and then uses the dictionary to create a new measurement in the database
    :param db_session: This is the database session that we created in the main.py file
    :type db_session: Session
    :param measurement: This is the measurement object that we want to add to the database
    :type measurement: Measurement
    :return: The db_measurement is being returned.
    """
    measurement = measurement.dict()
    try:
        db_measurement = models.MeasurementModels(
            device_id=measurement['device_id'],
            measurement=measurement['measurement'],
            timestamp=measurement['timestamp'],
        )
        db_session.add(db_measurement)
        db_session.commit()
        db_session.refresh(db_measurement)
    except Exception as error:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"""Error creating employee {error}"""
        )
    return db_measurement


def get_measurements(db_session: Session):
    """
    Get all the measurements from the database.
    :param db_session: Session
    :type db_session: Session
    :return: A list of all the measurements in the database.
    """
    return db_session.query(models.MeasurementModels).all()


def get_measurement(db_session: Session, id_measurement: int):
    """
    It gets a measurement from the database
    :param db_session: This is the database session that we created in the main.py file
    :type db_session: Session
    :param id_measurement: The id of the measurement to get
    :type id_measurement: int
    :return: the measurement with the id that is passed in.
    """
    db_measurement = db_session.query(models.MeasurementModels).filter(
        models.MeasurementModels.id == id_measurement).first()
    if not db_measurement:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The measurement with the id does not exist {id_measurement}"
        )
    return db_measurement


def update_measurement(db_session: Session, measurement: Measurement, id_measurement: int):
    """
    It updates a measurement in the database
    :param db_session: Session: The database session
    :type db_session: Session
    :param measurement: The measurement object that will be updated
    :type measurement: Measurement
    :param id_measurement: The id of the measurement to be updated
    :type id_measurement: int
    :return: The measurement updated correctly
    """
    db_measurement_found = db_session.query(models.MeasurementModels).filter(
        models.MeasurementModels.id == id_measurement)
    if not db_measurement_found.first():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The measurement with the id does not exist {id_measurement}"
        )
    try:
        db_measurement_found.update(measurement.dict(exclude_unset=True))
        db_session.commit()
    except Exception as error:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"""Error update employee {error}"""
        )
    return {"detail": "The measurement updated correctly"}


def delete_measurement(db_session: Session, id_measurement: int):
    """
    It deletes a measurement from the database
    :param db_session: The database session object
    :type db_session: Session
    :param id_measurement: The id of the measurement to be deleted
    :type id_measurement: int
    :return: a list of dictionaries with the information of the measurements.
    """
    db_measurement_found = db_session.query(models.MeasurementModels).filter(
        models.MeasurementModels.id == id_measurement)
    if not db_measurement_found.first():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"The measurement with the id does not exist {id_measurement}"
        )
    try:
        db_measurement_found.delete(synchronize_session=False)
        db_session.commit()
    except Exception as error:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"""Error delete employee {error}"""
        )
    return {"detail": "The measurement delete correctly"}


def generate_csv(db_session: Session, days: int):
    """
    > This function takes a database session and a number of days as input, and returns a dictionary
    with the name of the generated CSV file
    :param db_session: The database session to use for querying
    :type db_session: Session
    :param days: The number of days to generate the CSV for
    :type days: int
    :return: The name of the csv file.
    """
    today = datetime.now()
    start_date = today - timedelta(days=days)
    end_date = today
    measurements = db_session.query(models.MeasurementModels).filter(
        models.MeasurementModels.created_at >= start_date).filter(
            models.MeasurementModels.created_at < end_date).all()
    measurement_list = []
    for measurement in measurements:
        data = {
            "id": measurement.id,
            "device_id": measurement.device_id,
            "measurement": measurement.measurement,
            "timestamp": measurement.timestamp,
            "created_at": measurement.created_at.strftime('%Y-%m-%d %H:%M:%S'),
            "updated_at": measurement.updated_at.strftime('%Y-%m-%d %H:%M:%S')
        }
        measurement_list.append(data)
    data_frame = pd.DataFrame(measurement_list,
                              columns=[
                                  "id",
                                  "device_id",
                                  "measurement",
                                  "timestamp",
                                  "created_at",
                                  "updated_at"
                              ])
    name_csv = f"./csv/measurements-{start_date.strftime('%Y-%m-%d')}-{end_date.strftime('%Y-%m-%d')}.csv"
    data_frame.to_csv(name_csv, index=False)
    return {"name": name_csv.replace("./csv/", "")}


def to_int(value):
    """
    It takes a string, removes the "kwh" substring, and converts the result to an integer
    :param value: The value to be converted
    :return: the integer value of the string.
    """
    return int(value.replace("kwh", ""))


def data_analytics(body_analytics: Analytics):
    """
    It reads a CSV file, calculates some statistics, and plots the results
    :param body_analytics: Analytics
    :type body_analytics: Analytics
    :return: a dictionary with the detail of the graph.
    """
    body_analytics = body_analytics.dict()
    name_csv = body_analytics['name_csv']
    data_frame = pd.read_csv(f"./csv/{name_csv}")
    data_frame["measurement"] = data_frame["measurement"].apply(to_int)
    detail = "Generate graph correctly"
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1)
    # Calculate the number that is most often repeated in the reported range
    most_common_measurement = data_frame["measurement"].mode()[0]
    # Calculate how many data have been reported in 1 minute
    data_per_minute = data_frame[data_frame["timestamp"] >= (
        data_frame["timestamp"].max() - 60)].shape[0]
    data_per_minute_int = int(data_per_minute)
    # Calculate the average measure reported for each device
    mean_by_device = data_frame.groupby("device_id")["measurement"].mean()
    # Calculate the number of measures reported per device
    count_by_device = data_frame.groupby(
        "device_id")["measurement"].count()
    # Calculate the number of measures reported per hour
    data_frame["timestamp"] = pd.to_datetime(
        data_frame["timestamp"], unit="s")
    data_frame.set_index(data_frame["timestamp"], inplace=True)
    count_by_hour = data_frame.resample("H", on="timestamp")[
        "measurement"].count()
    ax1.bar(["Most common measurement", "Data per minute"],
            [most_common_measurement, data_per_minute_int])
    ax1.set_title("Calculate how many data have been reported in 1 minute")
    ax2.bar(mean_by_device.index, mean_by_device.values)
    ax2.set_title("Mean measurement by device")

    ax3.bar(count_by_device.index, count_by_device.values)
    ax3.set_title("Number of measurements by device")

    ax4.plot(count_by_hour.index, count_by_hour.values)
    ax4.set_title("Number of measurements by hour")

    fig.tight_layout()
    plt.show()
    return {"detail": detail}
