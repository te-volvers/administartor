from datetime import datetime
from sqlalchemy import Column, String, DateTime,Integer
from app.db.database import Base



class MeasurementModels(Base):
    """
    The Measurement class is a Python class that inherits from the Base class. It has a
    table name of "measurements", and it has four columns: id, device_id, measurement, and timestamp
    """
    __tablename__ = "employees"
    id = Column(Integer, primary_key=True, index=True)
    device_id = Column(String, index=True)
    measurement = Column(String, index=True)
    timestamp = Column(String)
    created_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
