from pydantic import BaseModel
from pydantic import Field


class Measurement(BaseModel):

    device_id: str = Field(..., example='abc3')
    measurement: str = Field(..., example='18 kwh')
    timestamp: str = Field(..., example='1672930258')

    class Config:
        orm_mode = True


class Analytics(BaseModel):

    name_csv: str = Field(...,
                          example='measurements-2023-01-01-2023-01-06.csv')
