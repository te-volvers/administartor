from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.db.database import Base, engine
from app.measurement.routers import measurement
from fastapi_pagination import add_pagination

origins = [
    "http://localhost",
    "http://localhost:8000",
]
DESCRIPTION = """
TE-VOLVERS API  test. 🚀

# Prueba Técnica Vacante DEV Python - TEVOLVERS
## Measurement

"""


def create_database():
    """
    It creates a database
    """
    Base.metadata.create_all(bind=engine)


create_database()


app = FastAPI(
    title="TE-VOLVERS",
    description=DESCRIPTION,
    version="0.0.1",
    terms_of_service="https://www.notion.so/TE-VOLVERS-2128368f7ae34bc48f0d7cc8a57eb809",
    contact={
        "name": "Hernando Escobar",
        "url": "https://gitlab.com/te-volvers",
        "email": "hernandoescobar23@gmail.com",
    },
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

add_pagination(app)
app.include_router(measurement.router)
